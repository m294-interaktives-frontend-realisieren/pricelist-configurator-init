const NR_OF_ROWS_PRICE_LIST = 10;
let products = [];

function createCellEl(content, tag) {
  let elCell = document.createElement(tag);
  elCell.innerHTML = content;
  return elCell;
}

function createRowEl(entries, isHead) {
  let elRow = document.createElement('tr');
  for (let entry of entries) {
    elRow.appendChild(createCellEl(entry, isHead ? 'th' : 'td'));
  }
  return elRow;
}

function createPriceList(products) {
  // Ihr Code hier
}

function updateSectionToDelete(products) {
  // Ihr Code hier
}

function updateData(products) {
  updateSectionToDelete(products);
  createPriceList(products);
}

function removeProduct(prodIndex) {
  // Ihr Code hier
}

function addProduct() {
  // Ihr Code hier
}
